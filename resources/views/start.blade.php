@extends('layouts.app')

@section('title', 'Start')


@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

@if(isset($status))
	<pre>{{ print_r($status); }}</pre>
	@foreach($status as $state)
	<div class="alert alert-{{ $state['type'] }}" role="alert">
		{{ $state['msg']  }}
	</div>
	@endforeach
@endif


@section('graveyard')
	@if(count($gameData['graveyard']->dominoes) > 0)
    <div id="graveyard">
		<h2>{{ $gameData['graveyard']->name }}</h2>
		<div class="dominoes">
			@foreach($gameData['graveyard']->dominoes as $key => $domino)
			<a href="{{ route('graveyardPick', ['player' => $gameData['activeplayer'], 'domino' => $key]) }}" class="domino">
				<div class="dots">{{ $domino->left_dots }}</div>
				<div class="dots">{{ $domino->right_dots }}</div>
			</a>
			@endforeach
		</div>
	</div>
	@endif
@endsection

@section('playField')
    <div id="playfield">
		<div class="dominoes">
			@foreach($gameData['playfield']->dominoes as $domino)
			<div class="domino">
				<div class="dots">{{ $domino->left_dots }}</div>
				<div class="dots">{{ $domino->right_dots }}</div>
			</div>
			@endforeach
		</div>
	</div>
@endsection

@section('players')
    <div id="player-dashboard">
		<div class="players">

			@foreach($gameData['players'] as $player)

				@if($gameData['activeplayer'] == $player->index && $player->state == 'winner')
				<div class="player active winner" style="width: {{ (100 / count($gameData['players'])) }}%">
				@elseif($player->state == 'winner')
				<div class="player winner" style="width: {{ (100 / count($gameData['players'])) }}%">
				@elseif($gameData['activeplayer'] == $player->index)
				<div class="player active" style="width: {{ (100 / count($gameData['players'])) }}%">
				@else
				<div class="player" style="width: {{ (100 / count($gameData['players'])) }}%">
				@endif

					<div class="winner-notice"><h3>Winner</h3></div>

					<h2>{{ $player->name }} - <small>{{ $player->score }}</small></h2>

					<div class="dominoes">
						@foreach($player->dominoes as $key => $domino)
						<a href="{{ route('playDomino', ['player' => $player->index, 'domino' => $key]) }}" class="domino">
							<div class="dots">{{ $domino->left_dots }}</div>
							<div class="dots">{{ $domino->right_dots }}</div>
						</a>
						@endforeach
					</div>

					@if( empty($gameData['graveyard']->dominoes) )
					<a href="{{ route('skipTurn', ['player' => $player->index]) }}" class="btn btn-danger">Skip Turn</a>
					@endif
					
				</div>
			@endforeach
		</div>
		<div id="restart"><a href="{{ route('restart') }}" class="btn btn-primary">Restart</a></div>
	</div>
@endsection

