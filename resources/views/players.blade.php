@extends('layouts.app')

@section('title', 'Player Select')

@section('playerSelect')
	<div id="choose-players" class="d-flex justify-content-center align-items-center">
		<div class="col-6">
			<form method="POST" action="/loading">
				@csrf
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<label class="input-group-text" for="player_select">Players: </label>
					</div>
					<select name="player_select" class="custom-select" id="player_select">
						<option selected>Choose...</option>
						<option value="2">Two</option>
						<option value="3">Three</option>
						<option value="4">Four</option>
					</select>
					<div class="input-group-append">
						<button class="btn btn-primary" type="submit">Start</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection