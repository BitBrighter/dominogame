<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Session;
use App\Classes\Graveyard;
use App\Classes\Player;
use App\Classes\PlayField;

class GameController extends Controller
{
    private $gameData;

    public function __construct(){

        $this->gameData = [
            'graveyard' => [],
            'playfield' => [],
            'players' => [],
            'activeplayer' => NULL,
            'winner' => NULL
        ];

    }

    public function index(Request $request){
        return view('players');
    }

    public function restart(Request $request){
        $request->session()->flush();
        return redirect('/');
    }

    public function loading(Request $request){

        if($request->session()->has('gameData')){

            $this->gameData = $request->session()->get('gameData');

        } else{

            $graveyard = new Graveyard();
            $graveyard->populateDominoes();

            $totalplayers = $request->input('player_select');
            for($i=0; $i<$totalplayers; $i++){
                
                $player = new Player($i);
                $player->addDominoes( $graveyard->dealDominoes(7) );
                $this->gameData['players'][] = $player;

            }

            // determine who goes first.
            $playersTemp = $this->gameData['players'];
            usort($playersTemp, function($a, $b){
                return $a->highestDouble < $b->highestDouble;
            });
            $this->gameData['activeplayer'] = $playersTemp[0]->index;

            $this->gameData['graveyard'] = $graveyard;
            $this->gameData['playfield'] = new PlayField();

            $request->session()->put('gameData', $this->gameData);

        }

        $gameData = $this->gameData;

        return redirect('/start');

    }

    public function start(Request $request){

        if($request->session()->has('gameData')){
            
            $gameData = $request->session()->get('gameData');

            // score each player.
            foreach($gameData['players'] as $player){
                $player->calcScores();
            }

            if(empty($gameData['graveyard']->dominoes)){
                
                // validate each player's dominoes to determine if the game can continue.
                $canGameContinue = false;
                $playerDominoes = [];
                foreach($gameData['players'] as $player){
                    foreach($player->dominoes as $domino){
                        $playerDominoes[] = $domino;
                    }
                }
                foreach($playerDominoes as $domino){
                    if($gameData['playfield']->validateDomino($domino)){
                        $canGameContinue = true;
                    }
                }

                if(!$canGameContinue){

                    // sort players by lowest score
                    usort($gameData['players'], function($a, $b){
                        return $a->score > $b->score;
                    });
                    
                    // declare winner with lowest score
                    $gameData['winner'] = $gameData['players'][0]->index;
                    $gameData['players'][0]->state = 'winner';

                    // save changes to session.
                    $request->session()->put('gameData', $gameData);

                }

            }

            return view('start', compact('gameData'));

        } else {
            return redirect('/');
        }

    }

    public function playDomino(Request $request){

        $playerIndex = $request->input('player');
        $dominoPlayedIndex = $request->input('domino');

        if($request->session()->has('gameData')){

            $gameData = $request->session()->get('gameData');

            // check if winner has been declared.
            if( is_null($gameData['winner']) ){

                // only allow the active player to place a domino.
                if($gameData['activeplayer'] == $playerIndex){

                    // move domino from player's hand to playing field.
                    $dominoPlayed = $gameData['players'][$playerIndex]->dominoes[$dominoPlayedIndex];
                    if($gameData['playfield']->validateDomino($dominoPlayed)){

                        $gameData['playfield']->addDomino($dominoPlayed);

                        unset($gameData['players'][$playerIndex]->dominoes[$dominoPlayedIndex]);
                        
                        // check if player has dominoes left to play.
                        if( empty($gameData['players'][$playerIndex]->dominoes) ){

                            $gameData['winner'] = $gameData['players'][$playerIndex];
                            $gameData['players'][$playerIndex]->state = 'winner';

                        } else {

                            // set next player's turn.
                            $playerIndex++;
                            $gameData['activeplayer'] = ($playerIndex >= count($gameData['players']))? 0: $playerIndex;

                        }

                        // save changes to session.
                        $request->session()->put('gameData', $gameData);
                    }

                }

            }

            return redirect('/start');

        } else {
            return redirect('/');
        }
    }

    public function graveyardPick(Request $request){

        $playerIndex = $request->input('player');
        $dominoIndex = $request->input('domino');

        if($request->session()->has('gameData')){

            $gameData = $request->session()->get('gameData');

            // check if winner has been declared.
            if( is_null($gameData['winner']) ){

                // move domino from graveyard to players hand.
                $dominoChosen = $gameData['graveyard']->dominoes[$dominoIndex]; 
                $gameData['players'][$playerIndex]->addDomino($dominoChosen);
                unset($gameData['graveyard']->dominoes[$dominoIndex]);

                // set next player's turn.
                $playerIndex++;
                $gameData['activeplayer'] = ($playerIndex >= count($gameData['players']))? 0: $playerIndex;

                // save changes to session.
                $request->session()->put('gameData', $gameData);

            }
            return redirect('/start');
        
        } else {
            return redirect('/');
        }
    }

    public function skipTurn(Request $request){

        $playerIndex = $request->input('player');

        if($request->session()->has('gameData')){

            $gameData = $request->session()->get('gameData');

            // check if winner has been declared.
            if( is_null($gameData['winner']) ){

                // set next player's turn.
                $playerIndex++;
                $gameData['activeplayer'] = ($playerIndex >= count($gameData['players']))? 0: $playerIndex;

                // save changes to session.
                $request->session()->put('gameData', $gameData);

            }

            return redirect('/start');

        } else {
            return redirect('/');
        }

    }


}
