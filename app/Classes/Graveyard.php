<?php
namespace App\Classes;

use App\Classes\Domino;
use Log;

class Graveyard {

    public $name;
    public $dominoes;

    public function __construct() {

		$this->name = 'Graveyard';
		$this->dominoes = [];

    }

    public function populateDominoes() {

        $this->dominoes[] = new Domino(0,0);
        $this->dominoes[] = new Domino(1,0);
        $this->dominoes[] = new Domino(2,0);
        $this->dominoes[] = new Domino(3,0);
        $this->dominoes[] = new Domino(4,0);
        $this->dominoes[] = new Domino(5,0);
        $this->dominoes[] = new Domino(6,0);

        $this->dominoes[] = new Domino(1,1);
        $this->dominoes[] = new Domino(2,1);
        $this->dominoes[] = new Domino(3,1);
        $this->dominoes[] = new Domino(4,1);
        $this->dominoes[] = new Domino(5,1);
        $this->dominoes[] = new Domino(6,1);

        $this->dominoes[] = new Domino(2,2);
        $this->dominoes[] = new Domino(3,2);
        $this->dominoes[] = new Domino(4,2);
        $this->dominoes[] = new Domino(5,2);
        $this->dominoes[] = new Domino(6,2);

        $this->dominoes[] = new Domino(3,3);
        $this->dominoes[] = new Domino(4,3);
        $this->dominoes[] = new Domino(5,3);
        $this->dominoes[] = new Domino(6,3);

        $this->dominoes[] = new Domino(4,4);
        $this->dominoes[] = new Domino(5,4);
        $this->dominoes[] = new Domino(6,4);

        $this->dominoes[] = new Domino(5,5);
        $this->dominoes[] = new Domino(6,5);

        $this->dominoes[] = new Domino(6,6);

		$this->shuffleDominoes();

    }

	public function shuffleDominoes(){
		shuffle($this->dominoes);
	}

	public function dealDominoes($count){
		$popped = [];

		for($i=0; $i<$count; $i++){
			$popped[] = $this->dominoes[$i];
		}

		$this->dominoes = array_slice($this->dominoes, $count, count($this->dominoes)-1);

		return $popped;
	}

	public function getDominoes(){
		return $this->dominoes;
	}
}
