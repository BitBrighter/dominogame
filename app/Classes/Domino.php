<?php
namespace App\Classes;
class Domino {

	public $total_dots;
	public $left_dots;
	public $right_dots;

    public function __construct($ldots, $rdots) {
		$this->left_dots = $ldots;
		$this->right_dots = $rdots;
		$this->total_dots = ($ldots + $rdots);
    }

	public function swapDots(){
		$oldleft = $this->left_dots;
		$this->left_dots = $this->right_dots;
		$this->right_dots = $oldleft;
	}

}
