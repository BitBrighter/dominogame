<?php
namespace App\Classes;

use App\Classes\Domino;
use Log;

class Player {

	public $name;
	public $index;
	public $score;
	public $highestDouble;
	public $doubles;
	public $dominoes;
	public $state;

    public function __construct($index) {

		$this->index = $index;
		$this->name = 'Player '.($index + 1);
		$this->dominoes = [];
		$this->doubles = [];
		$this->score = 0;
		$this->state = 'player';

    }

	public function addDominoes( $dominoes ){
		$this->dominoes = $dominoes;

		foreach($dominoes as $domino){
			if($domino->left_dots == $domino->right_dots){
				$this->doubles[] = $domino->left_dots;
			}
		}
		if(!empty($this->doubles)){
			rsort($this->doubles, SORT_NUMERIC);
			$this->highestDouble = $this->doubles[0];
		} else {
			$this->highestDouble = NULL;
		}
	}

	public function addDomino( $domino ){
		$this->dominoes[] = $domino;
	}

	public function getDominoes(){
		return $this->dominoes;
	}

	public function calcScores(){
		$this->score = 0;
		foreach($this->dominoes as $domino){
			$this->score += $domino->total_dots;
		}
		return $this->score;
	}

}
