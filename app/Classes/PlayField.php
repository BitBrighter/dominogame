<?php
namespace App\Classes;

use App\Classes\Domino;
use Log;

class PlayField {

	public $dominoes;
	public $leftMatch;
	public $rightMatch;

    public function __construct() {
		$this->dominoes = [];
		$this->leftMatch = -1;
		$this->rightMatch = -1;
    }

	public function validateDomino($domino){
		if($this->leftMatch == -1 && $this->rightMatch == -1){
			if($domino->left_dots == $domino->right_dots){
				return true;
			}
		} else {
			if($this->rightMatch == $domino->left_dots || $this->rightMatch == $domino->right_dots){
				return true;
			} elseif($this->leftMatch == $domino->right_dots || $this->leftMatch == $domino->left_dots){
				return true;
			} else {
				return false;
			}
		}
	}

	public function addDomino( $domino ){

		if($this->leftMatch == -1 && $this->rightMatch == -1){
			$this->dominoes[] = $domino;
		} else {
			if($this->rightMatch == $domino->left_dots || $this->rightMatch == $domino->right_dots){
				if($this->rightMatch == $domino->right_dots){
					// swap domino dots to match playfield order.
					$domino->swapDots();
					$this->dominoes[] = $domino;
				} else {
					$this->dominoes[] = $domino;
				}
			} else {
				if($this->leftMatch == $domino->right_dots || $this->leftMatch == $domino->left_dots){
					if($this->leftMatch == $domino->left_dots){
						// swap domino dots to match playfield order.
						$domino->swapDots();
						array_unshift($this->dominoes, $domino);
					} else {
						array_unshift($this->dominoes, $domino);
					}
				} else {
					return false;
				}
			}
		}

		// determine left and right side matching for next domino placement.
		$this->leftMatch = $this->dominoes[0]->left_dots;
		$this->rightMatch = $this->dominoes[count($this->dominoes) - 1]->right_dots;

		return true;
	}

	public function getDominoes(){
		return $this->dominoes;
	}

}
