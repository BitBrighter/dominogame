# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Versions ###

* node v12.16.1
* npm v6.13.4
* php v7.4
* composer v2.1.3 
* laravel v8.57.0

### How do I get set up? ###

* Clone repository
* ```git clone git@bitbucket.org:BitBrighter/dominogame.git```
* ```npm install```
* ```composer install```

### Setting up .env file ###
* Point ```YOUR_DOMAIN``` to laravel's public folder
* Open .env file
* Add ```YOUR_DOMAIN``` to the .env file
* ```APP_URL=YOUR_DOMAIN```
* Browse to ```YOUR_DOMAIN```

### Files Manually Created ###

* ```app/Http/Controllers/GameController.php```
* ```app/Classes/Domino.php```
* ```app/Classes/Graveyard.php```
* ```app/Classes/Player.php```
* ```app/Classes/PlayField.php```
* ```resources/views/layouts/app.blade.php```
* ```resources/views/players.blade.php```
* ```resources/views/start.blade.php```

### Who do I talk to? ###

* Repo owner or admin
* ```hannesbrink@gmail.com```