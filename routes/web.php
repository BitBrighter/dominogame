<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [GameController::class, 'index'])->name('index');
Route::get('/restart', [GameController::class, 'restart'])->name('restart');
Route::get('/start', [GameController::class, 'start'])->name('start');
Route::get('/play-domino', [GameController::class, 'playDomino'])->name('playDomino');
Route::get('/graveyard-pick', [GameController::class, 'graveyardPick'])->name('graveyardPick');
Route::get('/skip-turn', [GameController::class, 'skipTurn'])->name('skipTurn');

Route::post('/loading', [GameController::class, 'loading'])->name('loading');
